const express = require("express");
const app = express();
const port = 3000;
const fs = require("fs");
let users = require("./data_login.json");
let data = require("./data_user.json");
// let posts = require('./posts.json');

app.set("view engine", "ejs");

app.use(express.static("node_modules"));
app.use(express.static(__dirname + "/views"));
app.use(express.json());

app.get("/", (request, response) => {
  response.render("index", {
    name: users[0].username,
    data: data,
  });
});

app.get("/play", (request, response) => {
  response.render("index2", {
    title: "ROCK PAPER SCISSORS",
    displayId: true,
    viewOnly: false,
  });
});

app.post("/login", (request, response) => {
  // terima data yg dikirim oleh FE
  const { username, password } = request.body;
  //   console.log({ username, password });
  //   cek apakah data yg kita terima dari FE, ada ga sama data yg ada di BE
  const user = users.find((item) => item.username === username);
  if (user) {
    if (user.password === password) {
      response.status(200).json({ status: "logged-in", ...user });
    } else {
      response.status(403).json({ status: "invalid-password" });
    }
  } else {
    response.status(404).json({ status: "no-username" });
  }
});

// app.get('/api/posts', (request, response) => {
//     response.status(200).json(posts);
// });

app.listen(port, () => {
  console.log("Aplikasi berhasil dijalankan dengan port: ", port);
});

class ComputerPlayer extends Player{
    constructor(props){
        super(props);
    }
    
    async choose(){
        const com = document.getElementById("computerChoice");
        const ComOption = com.getElementsByClassName("options");
        const angkaRandom = Math.floor(Math.random()*ComOption.length);
        this.choice = await new Promise((resolve) => {
            const compChoice = ComOption[angkaRandom].dataset.option;
            ComOption[angkaRandom].classList.add("active");
            resolve(compChoice);
        });  
    }
}